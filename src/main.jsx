import React from 'react'
import ReactDOM from 'react-dom/client'
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer } from 'react-toastify';
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import { Route, Routes } from 'react-router-dom'

import store from './app/store'
import App from './App.jsx'
import Navigation from './Navigation/Navigation.jsx'
import './index.css'
import Register from './Authentication/Register'
import Login from './Authentication/LogIn'



ReactDOM.createRoot(document.getElementById('root')).render(
    <React.StrictMode>
        <Provider store={store}>
            <BrowserRouter>
                <Navigation />
                <Routes>
                    <Route path="/*" element={<App />} />
                    <Route path="/login" element={<Login />} />
                    <Route path="/register" element={<Register />} />
                </Routes>
                <ToastContainer
                    position="top-center"
                    autoClose={1500}
                    hideProgressBar={true}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                    theme="colored"
                />
            </BrowserRouter>
        </Provider>
    </React.StrictMode>
)
