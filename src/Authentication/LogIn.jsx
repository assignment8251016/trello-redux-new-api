import axios from 'axios';
import { Form, Button, Container, Row } from 'react-bootstrap';
import { useSelector, useDispatch } from 'react-redux'
import { useState, useRef, useEffect } from 'react';
import { useNavigate } from "react-router-dom";
import { toast } from 'react-toastify'

import backend from '../../config'
export default function Login() {
  const [show, setShow]  = useState(false)
  const navigate = useNavigate()
  const formRef = useRef(null);

  

  return (
    <Container
      fluid
      style={{
        backgroundColor: 'rgb(244, 244, 244)',
        paddingLeft: '5%',
        paddingRight: '5%',
      }}
    >
      <Row  id='register-row' style={{ justifyContent: "center", alignItems: "center", height: "80%" }} lg={1}>

        <Form
          ref={formRef}
          style={{
            backgroundColor: 'rgb(244, 244, 244)',
            maxWidth: "450px"

          }}>
          <Form.Group className="mb-4" >
            <Form.Label style={{ color: "#2D4B67", fontWeight: "800", fontSize: "1.3rem" }}>Email address</Form.Label>
            <Form.Control style={{ fontSize: "1.2rem", height: "3rem" }} type="email" placeholder="Enter email" id="email" autoComplete='username' />
          </Form.Group>

          <Form.Group className="mb-5" >
            <Form.Label style={{ color: "#2D4B67", fontWeight: "800", fontSize: "1.3rem" }}>Password</Form.Label>
            <Form.Control style={{ fontSize: "1.2rem", height: "3rem" }} type="password" placeholder="Enter password" id="password" autoComplete='current-password' />
          </Form.Group>
          <Button onClick={
            () => {
              setShow(true)
              const email = formRef.current.children[0].children[1].value
              const password = formRef.current.children[1].children[1].value

              if (password.length === 0) {
                toast.info("Please provide the password")
                setShow(false)
              }
              if (email.length === 0) {
                toast.info("Please provide the emial.")
                setShow(false)
              }

              email.length!==0 && password.length!==0 && doLogin(email, password, navigate).then().finally(()=>setShow(false))

            }} id="signin" variant="primary" size="lg" >
            <span style={{ display: !show ? "block" : "none" }}>Sign in</span>
            <svg id='loading' style={{ display: show ? "block" : "none" }} width="147px" height="147px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" >
              <circle cx="50" cy="50" fill="none" stroke="#2f8fff" strokeWidth="5" r="25" strokeDasharray="117.80972450961724 41.269908169872416">
                <animateTransform attributeName="transform" type="rotate" repeatCount="indefinite" dur="0.9900990099009901s" values="0 50 50;360 50 50" keyTimes="0;1" />
              </circle>
            </svg>
          </Button>
        </Form>

      </Row>
    </Container>
  );
}


function doLogin(email, password, navigate) {
  console.log(backend)
  return axios
    .post(
      `${backend}/login`, { email, password }
    ).then((response) => {
      localStorage.setItem('token', response.data.token)
      navigate('/')
    }).catch((error) => {
      toast.error(error.response? error.response.data.message.toString(): error.toString())
      localStorage.removeItem('token')
    })
}