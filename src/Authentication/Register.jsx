import axios from 'axios';
import { faL } from '@fortawesome/free-solid-svg-icons';
import { useRef, useState } from 'react';
import { Form, Button, Container, Row } from 'react-bootstrap';
import { useNavigate } from "react-router-dom";
import { toast } from 'react-toastify';
import backend from '../../config'
export default function Register() { 
  const [show, setShow] = useState(false)
  const navigate = useNavigate()
  const formRef = useRef(null);

  return (
    <Container
    fluid
    style={{
        backgroundColor: 'rgb(244, 244, 244)',
        paddingLeft: '5%',
        paddingRight: '5%',
    }}
>
    <Row style={{justifyContent:"center", alignItems:"center", height:"80%"}}  lg={1}>
        <Form
           ref={formRef}
          style={{
          backgroundColor: 'rgb(244, 244, 244)',
          maxWidth: "450px"
      
        }}>
              <Form.Group className="mb-4"  size="lg" >
                <Form.Label style={{color: "#2D4B67", fontWeight: "800",fontSize:"1.3rem"}}>Name</Form.Label>
                <Form.Control  style={{fontSize:"1.2rem", height:"3rem"}} type="text" placeholder="Enter name" />
              </Form.Group>
              
              <Form.Group className="mb-4" >
                <Form.Label style={{color: "#2D4B67", fontWeight: "800",fontSize:"1.3rem"}}>Email address</Form.Label>
                <Form.Control  style={{fontSize:"1.2rem", height:"3rem"}} type="email" placeholder="Enter email"   autocomplete='username'/>
              </Form.Group>
              
              <Form.Group className="mb-5" >
                <Form.Label style={{color: "#2D4B67", fontWeight: "800",fontSize:"1.3rem"}}>Password</Form.Label>
                <Form.Control style={{fontSize:"1.2rem", height:"3rem"}} type="password" placeholder="Enter password"  autocomplete='current-password' />
              </Form.Group>
              <Button onClick={() => {
                const username = formRef.current.children[0].children[1].value
                const email = formRef.current.children[1].children[1].value
                const password = formRef.current.children[2].children[1].value
            
                doRegister(username, email, password, navigate, setShow)
              } } id="register" variant="success" size="lg">
            <span style={{display: !show?"block":"none"}} >Register</span>
            <svg id='loading' style={{display: show?"block":"none"}}  width="147px" height="147px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" >
              <circle cx="50" cy="50" fill="none" stroke="#2f8fff" strokeWidth="5" r="25" strokeDasharray="117.80972450961724 41.269908169872416">
                <animateTransform attributeName="transform" type="rotate" repeatCount="indefinite" dur="0.9900990099009901s" values="0 50 50;360 50 50" keyTimes="0;1"/>
              </circle>
            </svg>
          </Button>
      </Form>
    </Row>
</Container>
  );
}

function doRegister(username, email, password, navigate, setShow) {
  setShow(true)
  return axios
    .post(
      `${backend}/register`, { username, email, password }
  ).then((response) => {
    toast.info("user '"+response.data.username+"' created! Please sign in")
    console.log(response.data)
      setShow(true)
      navigate('/login')
    }).catch((error) => {
      toast.error(error.response ? error.response.data.message.toString() : error.toString())
      setShow(false)
    })
}