export default function Error() {
    return (
        <div className="error-container">
            <h1>Not Found!</h1>
        </div>
    )
}
