import { useState } from 'react'
import axios from 'axios'

import MyVerticallyCenteredModal from './Model/MyVerticallyCenteredModal'
import { ListGroup } from 'react-bootstrap'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircleXmark } from '@fortawesome/free-solid-svg-icons'


export default function CCard({ list, card, handleDeleteCard }) {
    const [modalShow, setModalShow] = useState(false)
    return (
        <>
            <ListGroup.Item key={card.id}>
                <div className="custom-card-row">
                    <p onClick={() => setModalShow(true)}>{card.name}</p>
                    <FontAwesomeIcon
                        icon={faCircleXmark}
                        onClick={handleDeleteCard}
                    />
                </div>
            </ListGroup.Item>

            <MyVerticallyCenteredModal
                show={modalShow}
                onHide={() => setModalShow(false)}
                card={card}
                listName={list.name}
            />
        </>
    )
}
