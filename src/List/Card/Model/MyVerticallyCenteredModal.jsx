import CheckList from '../Checklist/CheckList'
import axios from 'axios'
import {
    Modal,
    InputGroup,
    Breadcrumb,
    Form,
    Card,
    Button,
} from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faListCheck } from '@fortawesome/free-solid-svg-icons'
import { useSelector, useDispatch } from 'react-redux'
import { setCheckLists, removeCheckList, newCheckList } from '../../../app/features/AllCheckLists'
import { useEffect, useState } from 'react'
import backend from '../../../../config'



export default function MyVerticallyCenteredModal({
    show,
    onHide,
    card,
    listName,
}) {
    const [text, setText] = useState(undefined)
    const dispatch = useDispatch()
    const checklistsOnCards = useSelector(
        (state) => state.AllCheckLists
    ).checkLists

    const checklists = checklistsOnCards.filter(
        (checklist) => checklist.id === card.id
    )[0]?.data

    useEffect(() => {
        fetchCheckLists(card.id, dispatch)
    }, [])

    return (
        <Modal
            show={show}
            onHide={onHide}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <FontAwesomeIcon icon={faListCheck} />
                <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                        <Breadcrumb.Item>{listName}</Breadcrumb.Item>

                        <Breadcrumb.Item active> {card.name}</Breadcrumb.Item>
                    </ol>
                </nav>
            </Modal.Header>
            <Modal.Body>
                <Card align-content-between style={{ boxShadow: 'none' }}>
                    {checklists &&
                        checklists?.map((checklist) => (
                            <CheckList
                                key={checklist.id}
                                checklist={checklist}
                                deleteCheckList={() =>
                                    deleteCheckList(checklist.id, card.id, dispatch)
                                }
                            />
                        ))}
                </Card>
                <InputGroup>
                    <Form.Control
                        onChange={(e) => handleCheckList(e, setText)}
                        placeholder="Check list name"
                    />
                    <Button
                        variant="outline-secondary"
                        onClick={(e) =>
                            createCheckList(card.id, text, dispatch)
                        }
                    >
                        Add
                    </Button>
                </InputGroup>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={onHide}>Close</Button>
            </Modal.Footer>
        </Modal>
    )
}


function fetchCheckLists(idCard, dispatch) {

    axios
        .get(
            `${backend}/cards/${idCard}/checklists`, {
            headers: {
                Authorization: `Bearer ${localStorage.token}`
            }
        }
        )
        .then((response) => {
            dispatch(setCheckLists({ id: idCard, data: response.data }))
        })
}

function deleteCheckList(idChecklist, cardId, dispatch) {
    dispatch(removeCheckList({ idChecklist, cardId }))
    axios.delete(
        `${backend}/checklists/${idChecklist}`, {
        headers: {
            Authorization: `Bearer ${localStorage.token}`,
        }
    }
    )
}

function createCheckList(idCard, name, dispatch) {
    axios
        .post(
            `${backend}/cards/${idCard}/checklists?name=${name}`, {}, {
            headers: {
                Authorization: `Bearer ${localStorage.token}`,
            }
        }
        )
        .then((response) => {
            dispatch(newCheckList({ id: idCard, data: response.data }))
        })
}

function handleCheckList(e, setText) {
    setText(e.target.value)
}