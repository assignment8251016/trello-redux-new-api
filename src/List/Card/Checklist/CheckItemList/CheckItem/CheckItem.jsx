import { Form, Card, Button, Row } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircleXmark } from '@fortawesome/free-solid-svg-icons'
import { useEffect, useState } from 'react'
export default function CheckItem({
    checkitem,
    deleteCheckItem,
    updateCheckitem,
}) {


    return (
        <>
            <Form.Check
                type={`checkbox`}
                label={checkitem.name}
                defaultChecked={checkitem.status === 'complete' && true}
                onChange={(e) => {
                    if (e.target.checked) {
                        updateCheckitem(checkitem, true)
                    } else {
                        updateCheckitem(checkitem, false)
                    }
                }}
            />

            <FontAwesomeIcon
                icon={faCircleXmark}
                onClick={() => {
                    deleteCheckItem(checkitem.id)
                }}
            />
        </>
    )
}
