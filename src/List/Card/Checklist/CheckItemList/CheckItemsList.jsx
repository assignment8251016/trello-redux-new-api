import CheckItem from './CheckItem/CheckItem'

export default function CheckItemsList({
    checkitems,
    deleteCheckItem,
    updateCheckitem,
}) {
    return (
        <>
            {checkitems &&
                checkitems?.map((checkitem) => {
                    return (
                        <div key={checkitem.id} className="checkitem-row">
                            <CheckItem
                                checkitem={checkitem}
                                deleteCheckItem={deleteCheckItem}
                                updateCheckitem={updateCheckitem}
                            />
                        </div>
                    )
                })}
        </>
    )
}
