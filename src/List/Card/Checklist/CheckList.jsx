import { useEffect, useState } from 'react'

import { Form } from 'react-bootstrap'
import { ListGroup, Card } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircleXmark } from '@fortawesome/free-solid-svg-icons'
import {
    setCheckItems,
    removeCheckItem,
    newCheckItem,
    updateitem,
} from '../../../app/features/AllCheckItems'
import { toast } from 'react-toastify'
import { useSelector, useDispatch } from 'react-redux'

import CheckItemsList from './CheckItemList/CheckItemsList'
import axios from 'axios'
import backend from '../../../../config'


export default function CheckList({ checklist, deleteCheckList }) {
    const [text, setText] = useState(undefined)
    let checkitems = useSelector((state) => state.AllCheckItems).checkItems
    checkitems = checkitems.filter((list) => list.id === checklist.id)[0]
    checkitems = checkitems?.data
    const dispatch = useDispatch()
    useEffect(() => {
        // getCheckItems(checklist.id, dispatch)
        fetchCheckItems(checklist.id, dispatch)
        // console.log('use Effect of checklist')
    }, [])

    return (
        <ListGroup.Item>
            <Card className="checkitem">
                <FontAwesomeIcon
                    icon={faCircleXmark}
                    onClick={deleteCheckList}
                />
                <Card.Title className="mb-4">{checklist.name}</Card.Title>

                <CheckItemsList
                    checkitems={checkitems}
                    deleteCheckItem={(checkitemId) =>
                       deleteCheckItem(checklist.id, checkitemId, dispatch)
                    }
                    updateCheckitem={(checkitem, state) =>
                       updateCheckitem(checkitem, checklist, state, dispatch)
                    }
                />

                <Form.Control
                    className="add-check-item"
                    type="text"
                    id="inputPassword5"
                    placeholder="Press 'Enter' to add check item"
                    onKeyUp={(e) => {
                        setText(e.target.value)
                        if (e.key === 'Enter') {
                            addCheckItem(checklist.id, text, dispatch)
                        }
                    }}
                />
            </Card>
        </ListGroup.Item>
    )
}


function fetchCheckItems(idChecklist, dispatch) {
    console.log("fetch checkitem "+backend)
    axios
        .get(
            `${backend}/checklists/${idChecklist}/checkItems`, {

            headers: {
                Authorization: `Bearer ${localStorage.token}`,
            }
        }
        )
        .then((response) => {
            console.log("response data", response.data)
            dispatch(setCheckItems({ id: idChecklist, data: response.data }))
        }
        )
        .catch((error) =>  toast.error(error?error.response?error.response.data.message:error.toString(): error.toString()))
}

function deleteCheckItem(checkListId, checkItemId, dispatch){
    dispatch(removeCheckItem({ checkListId, checkItemId }))
    axios
        .delete(
            `${backend}/checkitems/${checkItemId}`, {
            headers: {
                Authorization: `Bearer ${localStorage.token}`,
            }
        }
        )
        .catch((error) => toast.error('Cannot perform delete, '+error.toString()))
}

function addCheckItem(checkListId, name, dispatch) {
    console.log("add checkitem "+backend)
    axios
        .post(
            `${backend}/checklists/${checkListId}/checkItems?name=${name}`, {},
            {
                headers: {
                    Authorization: `Bearer ${localStorage.token}`,
                }
            }
        )
        .then((response) =>
            dispatch(newCheckItem({ id: checkListId, data: response.data }))
        )
        .catch((error) => toast.error('cannot add a new'+ error.response?error.response.data.message: error.toString()))
}

function updateCheckitem(checkitem, checklist, state, dispatch){
    dispatch(
        updateitem({
            checklistId: checklist.id,
            checkItemId: checkitem.id,
            state,
        })
    )

    axios
        .put(
            `${backend}/${checkitem.id}?status=${state ? 'complete' : 'incomplete'}`
            , {}, {
                headers: {
                    Authorization: `Bearer ${localStorage.token}`,
                }
            })
        .catch((error) => toast.error('problem in update. ' + error.response? error.response.data.message:error.toString()))
}