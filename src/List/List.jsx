import { useEffect } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBoxArchive } from '@fortawesome/free-solid-svg-icons'
import { Card, ListGroup, InputGroup, Form } from 'react-bootstrap'
import { setCardLists, addCard, removeCard } from '../app/features/AllCardLists'
import { useSelector, useDispatch } from 'react-redux'

import CCard from './Card/CCard'
import axios from 'axios'
import backend from '../../config'
export default function List({ list, archiveList }) {
    const AllCardLists = useSelector((state) => state.AllCardLists.cardLists)
    const data = AllCardLists?.filter((cardlist) => cardlist.id === list.id)[0]
    const cards = data?.data

    const dispatch = useDispatch()
    useEffect(() => {
        fetchCards(list.id, dispatch)
    }, [])

    return (
        <Card className="list">
            <Card.Header className="list-name d-flex justify-content-between">
                {list.name}
                <FontAwesomeIcon onClick={archiveList} icon={faBoxArchive} />
            </Card.Header>
            <ListGroup key={list.id} container variant="flush">
                {cards?.map((card) => (
                    <CCard
                        key={card.id}
                        list={list}
                        card={card}
                        handleDeleteCard={() =>
                            handleDeleteCard(card, dispatch)
                        }
                    />
                ))}

                <ListGroup.Item>
                    <InputGroup
                        size="sm"
                        className="mb-3"
                        style={{ width: '90%', marginLeft: '10px' }}
                    >
                        <Form.Control
                            style={{ fontSize: '1.4rem' }}
                            placeholder="Press 'Enter' to add new card"
                            aria-label="Small"
                            aria-describedby="inputGroup-sizing-sm"
                            onKeyDown={(e) =>
                                handleAddCardToList(
                                    e,
                                    e.target.value,
                                    list.id,
                                    dispatch
                                )
                            }
                        />
                    </InputGroup>
                </ListGroup.Item>
            </ListGroup>
        </Card>
    )
}

function handleAddCardToList(event, name, listId, dispatch) {
    if (event.key === 'Enter' && name.length > 0) {
        addNewCard(name, listId, dispatch)    
    }
}

function handleDeleteCard(card, dispatch) {
    deleteCard(card, dispatch)
}


function fetchCards(listId, dispatch){
    axios
        .get(
            `${backend}/lists/${listId}/cards`, {
                headers: {
                    Authorization: `Bearer ${localStorage.token}`
                }
            }
        )
        .then((response) => {
            dispatch(setCardLists({ id: listId, data: response.data }))
        })
}

function addNewCard(name, listId, dispatch) {
    axios
        .post(
            `${backend}/lists/${listId}?name=${name}`,{}, {
                headers : {
                    Authorization: `Bearer ${localStorage.token}`,
                }
        }
        )
        .then((response) => {
            dispatch(addCard({ id: listId, data: response.data }))
        })
}

function deleteCard(card, dispatch){
    console.log('delete card from allcatrdlist', card)
    dispatch(removeCard({ cardId: card.id, listId: card.idList }))
    axios
        .delete(
            `${backend}/cards/${card.id}`, {
                headers : {
                    Authorization: `Bearer ${localStorage.token}`,
                }
        }
        )
        .then((response) => {
            console.log(response.data)
        })
}