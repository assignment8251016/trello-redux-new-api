import { Link, useNavigate } from 'react-router-dom'

import Container from 'react-bootstrap/Container'
import Navbar from 'react-bootstrap/Navbar'
import { Form, FormControl, Button } from 'react-bootstrap'
import { setLogin } from '../app/features/Auth';

export default function Navigation() {

    const navigate = useNavigate()
    return (
        <Navbar bg="light" expand="lg">
            <Container>
                <Link to="/">
                    <Navbar.Brand>
                        <img src="https://a.trellocdn.com/prgb/assets/d947df93bc055849898e.gif" />
                    </Navbar.Brand>
                </Link>

                <Form inline className="d-flex" style={{columnGap: "10px"}}>
                    <FormControl
                        type="text"
                        placeholder="Search"
                        className="me-2"
                    />
                    <Button variant="dark">Search</Button>
                    <Button variant="success" onClick={() => {
                        navigate('/register')
                    }}>Register</Button>
                    <Button variant="success" onClick={() => {

                        navigate('/login')
                        logout()
                    }}>{localStorage.token!== undefined? 'logout': 'login'}</Button>
                </Form>
            </Container>
        </Navbar>
    )
}

function logout() { 
    if (localStorage.token !== undefined) {
        localStorage.removeItem('token')
    }
}