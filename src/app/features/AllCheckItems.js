import { createSlice } from '@reduxjs/toolkit'

const checkItemsSlice = createSlice({
    name: 'AllCheckItems',
    initialState: {
        checkItems: [],
        isLoadingCheckItems: true,
        error: false,
    },

    reducers: {
        setCheckItems: (state, action) => {
            if (
                !state.checkItems.find(
                    (checklist) => checklist.id === action.payload.id
                )
            ) {
                state.checkItems.push(action.payload)

            }
        },

        removeCheckItem: (state, action) => {

            state.checkItems = state.checkItems.filter((checklist) => {
                if (checklist.id === action.payload.checkListId) {
                    checklist.data = checklist.data.filter(
                        (checkitem) =>
                            checkitem.id !== action.payload.checkItemId
                    )
                }

                return checklist
            })
        },

        newCheckItem: (state, action) => {

            state.checkItems = state.checkItems.map((checklist) => {
                if (checklist.id === action.payload.id) {
                    checklist.data.push(action.payload.data)
                }
                return checklist
            })
        },

        updateitem: (state, action) => {

            state.checkItems.map((checklist) => {
                if (checklist.id === action.payload.checklistId) {
                    checklist.data = checklist.data.map((checkitem) => {
                        if (checkitem.id === action.payload.checkItemId) {
                            checkitem.status = action.payload.state
                                ? 'complete'
                                : 'incomplete'

                        }
                        return checkitem
                    })
                }
                return checklist
            })
        },

    },
})

export const {
    setCheckItems,
    removeCheckItem,
    newCheckItem,
    updateitem,
} = checkItemsSlice.actions

export default checkItemsSlice.reducer
