import { faLessThanEqual } from '@fortawesome/free-solid-svg-icons'
import { createSlice } from '@reduxjs/toolkit'
import axios from 'axios'

const authSlice = createSlice({
    name: 'Auth',
    initialState: {
        id: undefined,
        username: undefined,
        token: undefined,
        login: false
    },

    reducers: {
        setAuth: (state, action) => {
            localStorage.setItem('token', action.payload.token)
            state.login = true
            state.id = action.payload.id
            state.username = action.payload.email
            state.token = action.payload.token
        },

        setLogin: (state, action) => {
            state.login = false
        }

    },
})

export  const { setAuth, setLogin } = authSlice.actions



export default authSlice.reducer