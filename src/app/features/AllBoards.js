import { createSlice } from '@reduxjs/toolkit'
import axios from 'axios'


const boardsSlice = createSlice({
    name: 'AllBoards',
    initialState: {
        boards: [],
        isLoadingBoards: true,
        error : undefined
    },

    reducers: {
        setBoards: (state, action) => {
            state.boards = action.payload.data
            state.isLoadingBoards = false
            state.error = action.payload.error
        },

        addBoard: (state, action) => {
            state.boards.push(action.payload)
        },
    },
})

export const { setBoards, addBoard } = boardsSlice.actions
export default boardsSlice.reducer
