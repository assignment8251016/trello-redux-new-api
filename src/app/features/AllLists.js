import { createSlice } from '@reduxjs/toolkit'
import axios from 'axios'



const listsSlice = createSlice({
    name: 'AllLists',
    initialState: {
        lists: [],
        isLoadingLists: true,
        isAddList: true,
        error: false,
    },

    reducers: {
        setLists: (state, action) => {
            state.lists = action.payload
            state.isLoadingLists = false
        },

        addList: (state, action) => {
            state.lists.push(action.payload)
        },

        archive: (state, action) => {
            state.lists = state.lists.filter(
                (list) => list.id !== action.payload
            )
        },
    },
})

export const { setLists, addList, archive } = listsSlice.actions


export default listsSlice.reducer
