import { createSlice } from '@reduxjs/toolkit'

const checkListsSlice = createSlice({
    name: 'AllCheckLists',
    initialState: {
        checkLists: [],
        isLoadingCheckLists: true,
        error: false,
    },

    reducers: {
        setCheckLists: (state, action) => {
            if (
                !state.checkLists.find((list) => list.id === action.payload.id)
            ) {
                state.checkLists.push(action.payload)
            }
            state.isLoadingCheckLists = false
        },

        removeCheckList: (state, action) => {
            state.checkLists = state.checkLists.filter((checklists) => {
                if (checklists.id === action.payload.cardId) {
                    // console.log("cardid", checklists.id, action.payload.cardId)
                    checklists.data = checklists.data.filter(
                        (checklist) => {
                            // console.log("filter",action.payload.idChecklist, checklist)
                            return checklist.id !== action.payload.idChecklist
                        }
                    )
                }
                return checklists
            })
        },

        newCheckList: (state, action) => {
            // console.log('creating new checlist')
            state.checkLists = state.checkLists.map((card) => {
                if (card.id === action.payload.id) {
                    card.data.push(action.payload.data)
                }
                return card
            })
        },
    },
})

export const { setCheckLists, removeCheckList, newCheckList } =
    checkListsSlice.actions

export default checkListsSlice.reducer
