import { createSlice } from '@reduxjs/toolkit'
import axios from 'axios'
import Auth from '../../../Auth'

const { Key, Token } = Auth

const cardListsSlice = createSlice({
    name: 'AllCardLists',
    initialState: {
        cardLists: [],
        isLoadingCards: true,
        error: false,
    },

    reducers: {
        setCardLists: (state, action) => {
            if (
                !state.cardLists.find((list) => list.id === action.payload.id)
            ) {
                state.cardLists.push(action.payload)
            }
            state.isLoadingCards = false
        },

        addCard: (state, action) => {
            state.cardLists = state.cardLists.map((list) => {
                if (list.id === action.payload.id) {
                    list.data.push(action.payload.data)
                }
                return list
            })
        },

        removeCard: (state, action) => {
            console.log("action",action.payload)
            state.cardLists = state.cardLists.filter((list) => {
                if (list.id === action.payload.listId) {
                    list.data = list.data.filter(
                        (card) => card.id !== action.payload.cardId
                    )
                }
                return list
            })
        },
    },
})

export const { setCardLists, addCard, removeCard } = cardListsSlice.actions


export default cardListsSlice.reducer
