import { configureStore } from '@reduxjs/toolkit'
import boardReducer from './features/AllBoards'
import listReducer from './features/AllLists'
import CardListReducer from './features/AllCardLists'
import ChecklistReducer from './features/AllCheckLists'
import checkItemReducer from './features/AllCheckItems'
import authReducer from './features/Auth'
export default configureStore({
    reducer: {
        AllBoards: boardReducer,
        AllLists: listReducer,
        AllCardLists: CardListReducer,
        AllCheckLists: ChecklistReducer,
        AllCheckItems: checkItemReducer,
        auth: authReducer,
    },
})
