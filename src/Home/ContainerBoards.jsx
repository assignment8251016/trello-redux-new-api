import { Container, Row } from 'react-bootstrap'
import { Form, InputGroup, Button } from 'react-bootstrap'
import Board from './Board/Board'
import { useState } from 'react'
import Loading from '../Loading/loading'
import { toast } from 'react-toastify'
export default function ({ boards, addAnotherBoard, found }) {
    const [name, setName] = useState(undefined)

    return (
        <Container
            fluid
            style={{
                backgroundColor: 'rgb(244, 244, 244)',
                paddingLeft: '5%',
                paddingRight: '5%',
            }}
        >
            {boards.length > 0 ? <Row >
                {

                    boards.map((board) => (
                        <Board
                            image={board.backgroundImage}
                            name={board.name}
                            boardId={board.id}
                        // color={board.backgroundColor}
                        />
                    ))

                }
                <InputGroup
                    className="mb-3 dragable-container"
                    style={{
                        marginTop: '10px',
                        width: '300px',
                        height: '50px',
                    }}
                >
                    <Form.Control
                        placeholder="Enter board name"
                        aria-label="list-name"
                        aria-describedby="basic-addon2"
                        onChange={(e) => {
                            setName(e.target.value)
                        }}
                    />
                    <Button
                        variant="outline-secondary"
                        id="button-addon2"
                        onClick={(event) => {
                            if (name) {
                                addAnotherBoard(name)
                            }
                        }}
                    >
                        + New Board
                    </Button>
                </InputGroup>
            </Row> : (
                    <>
                        <InputGroup
                    className="mb-3 dragable-container"
                    style={{
                        marginTop: '10px',
                        width: '300px',
                        height: '50px',
                    }}
                >
                    <Form.Control
                        placeholder="Enter board name"
                        aria-label="list-name"
                        aria-describedby="basic-addon2"
                        onChange={(e) => {
                            setName(e.target.value)
                        }}
                    />
                    <Button
                        variant="outline-secondary"
                        id="button-addon2"
                        onClick={(event) => {
                            if (name) {
                                addAnotherBoard(name)
                            }
                        }}
                    >
                        + New Board
                    </Button>
                </InputGroup>
                    <Loading found={found} />
                </>
            )}

        </Container>
    )
}
