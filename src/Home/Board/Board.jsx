import Button from 'react-bootstrap/Button'
import Card from 'react-bootstrap/Card'
import { Link, useNavigate } from 'react-router-dom'
export default function Board({ image, name, boardId, color }) {
    const navigate = useNavigate()
    return (
        <Card
            style={{
                minHeight: '180px',
                color: 'white',
                backgroundColor: '#0079BF ',
                backgroundImage: `url(${image})`,
            }}
            onClick={() =>
                navigate(`/${boardId}/${name}`, { state: { image } })
            }
        >
            <Card.Header style={{ border: 'none' }}>
                <Card.Title>{name}</Card.Title>
            </Card.Header>
        </Card>
    )
}
