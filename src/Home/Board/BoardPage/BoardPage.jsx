import { useEffect, useState } from 'react'
import { Button, Container, Row, InputGroup, Form } from 'react-bootstrap'
import { useParams, useLocation } from 'react-router-dom'
import { toast } from 'react-toastify'
import { useSelector, useDispatch } from 'react-redux'
import { setLists, addList, archive } from '../../../app/features/AllLists'

import List from '../../../List/List'
import Error from '../../../Error/Error'
import axios from 'axios'
import backend from '../../../../config'


export default function BoardPage() {

    const State = useSelector((state) => state.AllLists)
    const dispatch = useDispatch()

    const { state } = useLocation()
    const { image } = state || {
        image: 'https://www.bing.com/th?id=OHR.PlayfulHumpback_EN-IN6301739594_1920x1080.webp',
    }

    const { id } = useParams()

    const [name, setName] = useState(undefined)

    useEffect(() => {
        fetchLists(id, dispatch)
    }, [])

    return (
        <Container fluid style={{ backgroundImage: `url(${image})` }}>
            {!State?.error ? (
                <Row style={{ flexWrap: 'nowrap' }}>
                    {State?.lists?.length > 0 &&
                        State.lists.map((list) => {
                            return (
                                <List
                                    key={list.id}
                                    list={{ ...list, image }}
                                    boardId={id}
                                    archiveList={() =>
                                        archiveList(list.id, dispatch)
                                    }
                                />
                            )
                        })}

                    <InputGroup
                        className="mb-3 dragable-container"
                        style={{
                            marginTop: '10px',
                            width: '300px',
                            height: '50px',
                        }}
                    >
                        <Form.Control
                            placeholder="New list name"
                            aria-label="list-name"
                            aria-describedby="basic-addon2"
                            onChange={(e) => setName(e.target.value)}
                        />
                        <Button
                            variant="outline-secondary"
                            id="button-addon2"
                            onClick={(event) => {
                                if (name) {
                                    addNewList(id, name, dispatch)
                                }
                            }}
                        >
                            + New List
                        </Button>
                    </InputGroup>
                </Row>
            ) : (
                <Error />
            )}
        </Container>
    )
}

function fetchLists(boardId, dispatch) {
    axios
        .get(
            `${backend}/boards/${boardId}/lists`, {
            headers: {
                Authorization: `Bearer ${localStorage.token}`
            }
        }
        )
        .then((response) => dispatch(setLists(response.data))).catch(err => toast.error(err.response?err.response.data.message: err.toString()))
}

function addNewList(boardId, listName, dispatch) {
    axios
        .post(
            // `https://api.trello.com/1/boards/${boardId}/lists?name=${listName}&key=${Key}&token=${Token}`
            `${backend}/boards/${boardId}/${listName}`, {}, {
            headers: {
                Authorization: `Bearer ${localStorage.token}`,
            }
        }
        )
        .then((response) => dispatch(addList(response.data))).catch(error=>toast.error(error.response && error.response.data.message))
}

function archiveList(listId, dispatch) {
    dispatch(archive(listId))
    axios.put(
        `${backend}/lists/${listId}/closed`, {}, {
        headers: {
            Authorization: `Bearer ${localStorage.token}`,
        }
    }).catch(error=>toast.error(error.response && error.response.data.message))

}

