import 'bootstrap/dist/js/bootstrap.bundle.min'
import 'bootstrap/dist/css/bootstrap.min.css'
import axios from 'axios'

import ContainerBoards from './Home/ContainerBoards'
import BoardPage from './Home/Board/BoardPage/BoardPage'
import { useSelector, useDispatch } from 'react-redux'
import { setBoards, addBoard } from './app/features/AllBoards'
import { useNavigate, Route, Routes } from 'react-router-dom'
import { useEffect, useState } from 'react'
import { toast } from 'react-toastify'
import backend from '../config'

export default function App() {
    const boards = useSelector((state) => state.AllBoards)
    const navigate = useNavigate()
    const dispatch = useDispatch()
    const [found, setFound] = useState(true)
    useEffect(() => {
        fetchBoards(setFound,navigate, dispatch)
    }, [])

    return (
        <>
            { (
                <Routes>
                    <Route
                        path="/"
                        element={
                            <ContainerBoards
                                boards={boards.boards}
                                addAnotherBoard={(name) =>
                                    dispatch(addNewBoard(dispatch,name))
                                }
                                found={found}
                            />
                        }
                    />
                    <Route path="/:id/:name" element={<BoardPage />} />
                </Routes>
            ) 
               
            }

        </>
    )
}


function fetchBoards(setFound,navigate, dispatch) {

    axios
        .get(
            `${backend}/boards`, {
            headers: {
                Authorization: `Bearer ${localStorage.token}`,
            }
        }
        )
        .then((response) => {
            if (response.data.length === 0) {
                setFound(false)
                toast.info('Boards not found!')
            }
            dispatch(setBoards({ error: undefined, data: response.data }))
        })
        .catch(error => {
            dispatch(setBoards({ error: undefined, data:[] }))
            console.log("errr to fetch", error)
            toast((error.response ? error.response.data.message : error.toString())+'. Please login!')
            if (localStorage.token !== undefined) {
                localStorage.removeItem('token')
            }

            setTimeout(() => {
                navigate('/login')
            }, 1000);
        })
}

function addNewBoard(dispatch, name) {
    axios
        .post(
            `${backend}/boards?name=${name}&idUser=68122adb-6dff-429f-9ac8-e04726e78803`, {}, {
            headers: {
                Authorization: `Bearer ${localStorage.token}`,
            }
        }
        )
        .then((response) => dispatch(addBoard(response.data)))
        .catch(error => {
            toast(error.response ? error.response.data.message : error.toString())
        })
}
