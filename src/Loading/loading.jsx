export default function Loading({found}) {

    return (<div
        style={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            opacity: '.8',
            marginTop: '20%',
        }}
    >
        <h1 style={{ color: '#B6B6B4' }}>{found?'Loading ...':<>Use button '<span style={{ color: '#36454F' }}>+New Board</span>' to add a new board</>}</h1>
    </div>)
}